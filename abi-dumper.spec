Name: abi-dumper
Version: 1.4
Release: 1
Summary: a tool to dump ABI of an ELF object containing DWARF debug info.
URL: https://github.com/lvc/abi-dumper
License: LGPL-2.1-or-later
Source: https://github.com/lvc/%{name}/archive/%{version}/%{name}-%{version}.tar.gz
BuildRequires: gcc make git perl-interpreter perl-generators
Requires:      elfutils elfutils-devel libstdc++ libstdc++-devel

%description
The tool is intended to be used with ABI Compliance Checker tool for
tracking ABI changes of a C/C++ library or kernel module:
https://github.com/lvc/abi-compliance-checker

%prep
%autosetup -p1

%install
mkdir -p %{buildroot}%{_prefix}
perl Makefile.pl -install --prefix=%{_prefix} --destdir=%{buildroot}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

%changelog
* Sun Jan 12 2025 Funda Wang <fundawang@yeah.net> - 1.4-1
- update to 1.4

* Mon Nov 14 2022 caodongxia <caodongxia@h-partners.com> - 1.2-2
- Modify invalid source0

* Tue Jan 18 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.2-1
- Upgrade to version 1.2

* Sat Jul 25 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.1-1
- Package Init
